<b>Welcome to X-TomDoc</b>

A self-explaining Tomato Disease detection system that is trained to detect healthy and unhealthy tomato leaves attacked by late blight and Septoria leaf spot.

<b><u>STEPS TO RUN THE APPLICATION</b></u>

1. Clone this repository:
    > git clone https://gitlab.com/abutihere/x-tomdoc
2. Change the directory to x-tomdoc and install the virtualenv module (This is highly recommended).
    > cd x-TomDoc
    > pip install virtualenv
3. Create a virtual environment.
    > virtualenv env
4. Run the following to activate the newly created environment.
    > source env/scripts/activate
5. Install all dependencies by running the following:
    > pip install -r requirements.txt
6. Run the following to start the application
    > streamlit run 1_😀_Welcome.py (recommended to use the tab key after typing the underscore after "1" if typing the emoji is challenging)

<b><u>MODELS</b></u>

You may choose one of the nine models provided with this system. The description of each is given below:

1. Vgg16, Vgg19 and IncepV3 => models created by training only the top layer of VGG16, VGG19 and InceptionV3 from Keras libary with the <b>un-segmented</b> version of the PlantVillage dataset.

2. SVgg16, SVgg19 and SIncepV3 => models created by training only the top layer of VGG16, VGG19 and InceptionV3 from Keras libary with the <b>segmented</b> version of the PlantVillage dataset where the background color of all the images is <b>black</b>.

3. SNVgg16, SNVgg19 and SNIncepV3 => models created by training only the top layer of VGG16, VGG19 and InceptionV3 from Keras libary with the <b>segmented</b> version of the PlantVillage dataset where the background color of the images is made to be randomly choosen <b>variable colors</b>.

<b><u>NOTES</b></u>

The explanations are by default cached. Therefore, choosing a different model but with the same base-model does not invoke the explanation generation and you may obtain the same explanation for a different model(For example, switching between Vgg16 and SVgg16). Click the menu at the very right corner to clear the cache when it is required.

